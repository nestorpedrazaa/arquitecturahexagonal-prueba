<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function () {
    
    Route::post('login',[AuthController::class, 'login']);
    Route::post('logout',[AuthController::class, 'logout']);
});


Route::middleware(['auth:api'])->group(function () {
    Route:: post('/productoo/create', [ProductoController::class, 'store']);
    Route::put('/producto/update/{id}',[ProductoController::class, 'update']);
    Route::delete('/producto/delete/{id}',[ProductoController::class, 'destroy']);
    Route:: post('/producto/filter',[ProductoController::class, 'getById']);
    Route:: get('/producto/list',[ProductoController::class, 'getAll']);

    Route::post('/categoria/create', [CategoriaController::class, 'store']);
    Route::put('/categoria/update/{id}',[CategoriaController::class, 'update']);
    Route::delete('/categoria/delete/{id}', [CategoriaController::class, 'destroy']);
    Route:: post('/categoria/filter',[CategoriaController::class, 'getById']);
    Route:: get('/categoria/list',[CategoriaController::class, 'getAll']);

    Route::post('/user/create', [UserController::class, 'store']);
    Route::put('/user/update/{id}',[UserController::class, 'update']);
    Route::delete('/user/delete/{id}', [Userontroller::class, 'destroy']);
    Route:: post('/user/filter',[UserController::class, 'getById']);
    Route:: get('/user/list',[UserController::class, 'getAll']);
   
});