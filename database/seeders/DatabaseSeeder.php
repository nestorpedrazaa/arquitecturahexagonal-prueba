<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {

        User::factory()->create([
            'name' => 'Nestor Pedraza',
            'email' => 'nestorp@ejemplo.com',
            'password' => Hash::make('12345')
        ]);
    }
}
