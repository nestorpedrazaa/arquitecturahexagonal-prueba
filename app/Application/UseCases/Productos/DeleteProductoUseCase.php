<?php

namespace App\Application\UseCases\Productos;

use App\Core\Repositories\ProductoRepository;

class DeleteProductoUseCase
{
    private $productoRepository;

    public function __construct(ProductoRepository $productoRepository)
    {
        $this->productoRepository = $productoRepository;
    }

    public function execute($id){
        return $this->productoRepository->delete($id);
    }
}
