<?php

namespace App\Application\UseCases\Productos;

use App\Core\Repositories\ProductoRepository;

class CreateProductoUseCase
{
    private $productoRepository;

    public function __construct(ProductoRepository $productoRepository)
    {
        $this->productoRepository = $productoRepository;
    }

    public function execute($productoDate){
        return $this->productoRepository->create($productoDate);
    }
}
