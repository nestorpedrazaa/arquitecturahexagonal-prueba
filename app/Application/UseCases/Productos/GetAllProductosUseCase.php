<?php

namespace App\Application\UseCases\User;

use App\Core\Repositories\ProductoRepository;

class GetAllProductosUseCase {
    private $productoRepository;

    public function __construct(ProductoRepository $productoRepository)
    {
        $this->productoRepository = $productoRepository;
    }

    public function execute() {
        return $this->productoRepository->getAll();
    }
}
