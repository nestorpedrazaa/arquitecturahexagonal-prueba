<?php

namespace App\Application\UseCases\User;

use App\Core\Repositories\ProductoRepository;

class GetProductoByIdCase {
    private $productoRepository;

    public function __construct(ProductoRepository $productoRepository)
    {
        $this->productoRepository = $productoRepository;
    }

    public function execute(int $id) {
        return $this->productoRepository->findById($id);
    }
}
