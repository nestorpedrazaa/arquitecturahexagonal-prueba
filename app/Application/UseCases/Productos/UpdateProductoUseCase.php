<?php

namespace App\Application\UseCases\Productos;

use App\Core\Repositories\ProductoRepository;

class UpdateProductoUseCase
{
    private $productoRepository;

    public function __construct(ProductoRepository $productoRepository)
    {
        $this->productoRepository = $productoRepository;
    }

    public function execute($id,$productoDate){
        return $this->categoriaRepository->update($id,$productoDate);
    }
}
