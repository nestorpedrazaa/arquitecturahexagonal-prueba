<?php

namespace App\Application\UseCases\Categorias;

use App\Core\Repositories\CategoriaRepository;

class CreateCategoriaUseCase
{
    private $categoriaRepository;

    public function __construct(CategoriaRepository $categoriaRepository)
    {
        $this->categoriaRepository = $categoriaRepository;
    }

    public function execute($categoriaDate){
        return $this->categoriaRepository->create($categoriaDate);
    }
}
