<?php

namespace App\Application\UseCases\User;

use App\Core\Repositories\CategoriaRepository;

class GetCategoriaByIdCase {
    private $categoriaRepository;

    public function __construct(CategoriaRepository $categoriaRepository)
    {
        $this->categoriaRepository = $categoriaRepository;
    }

    public function execute(int $id) {
        return $this->categoriaRepository->findById($id);
    }
}
