<?php

namespace App\Application\UseCases\User;

use App\Core\Repositories\CategoriaRepository;

class GetAllCategoriasUseCase {
    private $categoriaRepository;

    public function __construct(CategoriaRepository $categoriaRepository)
    {
        $this->categoriaRepository = $categoriaRepository;
    }

    public function execute() {
        return $this->categoriaRepository->getAll();
    }
}
