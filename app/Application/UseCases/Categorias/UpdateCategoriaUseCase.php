<?php

namespace App\Application\UseCases\Categorias;

use App\Core\Repositories\CategoriaRepository;

class UpdateCategoriaUseCase
{
    private $categoriaRepository;

    public function __construct(CategoriaRepository $categoriaRepository)
    {
        $this->categoriaRepository = $categoriaRepository;
    }

    public function execute($id,$categoriaDate){
        return $this->categoriaRepository->update($id,$categoriaDate);
    }
}
