<?php

namespace App\Core\Entities;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'price',
        'stock',
    ];

    public function categorias()
    {
        return $this->belongsToMany(Categora::class, 'productos_categorias');
    }
}
