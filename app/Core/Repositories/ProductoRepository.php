<?php 

namespace App\Core\Repositories;

interface ProductoRepository{

    public function create($productoDate);
    public function update($id, $productoDate);
    public function delete($id);
}
