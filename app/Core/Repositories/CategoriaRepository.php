<?php

namespace App\Core\Repositories;

interface CategoriaRepository{

    public function create($categoriaDate);
    public function update($id, $categoriaDate);
    public function delete($id);
}
