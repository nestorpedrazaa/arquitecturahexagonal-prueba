<?php

namespace App\Http\Controllers;

use App\Application\UseCases\Categories\CreateUserUseCase;
use App\Application\UseCases\Categories\DeleteUserUseCase;
use App\Application\UseCases\Categories\UpdateUserUseCase;
use App\Application\UseCases\Categories\GetAllUsersUseCase;
use App\Application\UseCases\Categories\GetUserByIdUseCase;


use Illuminate\Http\Request;

class UserController extends Controller
{

    private $mensaje = 'Internal Server Error';
    
    public function store(Request $request,CreateUserUseCase $useCase)
    {
        try{
            return $useCase->execute($request);
        }catch (\Throwable $th){
            return response(["message" => $this->mensaje], 500);
        }
    }

    public function update(int $id, Request $request, UpdateUserUseCase $useCase)
    {
        try{
            return $useCase->execute($id, $request);
        }catch (\Throwable $th){
            return response(["message" => $this->mensaje], 500);
        }
    }

    public function destroy(int $id, DeleteUserUseCase $useCase)
    {
        try{
            return $useCase->execute($id);
        }catch (\Throwable $th){
            return response(["message" =>$this->mensaje], 500);
        }
    }

    public function getById(int $id, GetUserByIdUseCase $useCase)
    {
        try{
            return $useCase->execute($id);
        }catch (\Throwable $th){
            return response(["message" =>$this->mensaje], 500);
        }
    }

    public function getAll(GetAllUsersUseCase $useCase)
    {
        try{
            return $useCase->execute();
        }catch (\Throwable $th){
            return response(["message" =>$this->mensaje], 500);
        }
    }
}
