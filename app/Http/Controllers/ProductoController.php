<?php

namespace App\Http\Controllers;

use App\Application\UseCases\Categories\CreateProductoUseCase;
use App\Application\UseCases\Categories\DeleteProductoUseCase;
use App\Application\UseCases\Categories\UpdateProductoUseCase;
use Illuminate\Http\Request;

class ProductoController extends Controller
{

    private $mensaje = 'Internal Server Error';
    
    public function store(Request $request,CreateProductoUseCase $useCase)
    {
        try{
            return $useCase->execute($request);
        }catch (\Throwable $th){
            return response(["message" => $this->mensaje], 500);
        }
    }

    public function update(int $id, Request $request, UpdateProductoUseCase $useCase)
    {
        try{
            return $useCase->execute($id, $request);
        }catch (\Throwable $th){
            return response(["message" => $this->mensaje], 500);
        }
    }

    public function destroy(int $id, DeleteProductoUseCase $useCase)
    {
        try{
            return $useCase->execute($id);
        }catch (\Throwable $th){
            return response(["message" =>$this->mensaje], 500);
        }
    }

    public function getById(int $id, GetProductoByIdUseCase $useCase)
    {
        try{
            return $useCase->execute($id);
        }catch (\Throwable $th){
            return response(["message" =>$this->mensaje], 500);
        }
    }

    public function getAll(GetAllProductosUseCase $useCase)
    {
        try{
            return $useCase->execute();
        }catch (\Throwable $th){
            return response(["message" =>$this->mensaje], 500);
        }
    }
}
