<?php

namespace App\Http\Controllers;

use App\Application\UseCases\Categories\CreateCategoriaUseCase;
use App\Application\UseCases\Categories\DeleteCategoriaUseCase;
use App\Application\UseCases\Categories\UpdateCategoriaUseCase;
use Illuminate\Http\Request;

class CategoriaController extends Controller
{

    private $mensaje = 'Internal Server Error';
    
    public function store(Request $request,CreateCategoriaUseCase $useCase)
    {
        try{
            return $useCase->execute($request);
        }catch (\Throwable $th){
            return response(["message" => $this->mensaje], 500);
        }
    }

    public function update(int $id, Request $request, UpdateCategoriaUseCase $useCase)
    {
        try{
            return $useCase->execute($id, $request);
        }catch (\Throwable $th){
            return response(["message" => $this->mensaje], 500);
        }
    }

    public function destroy(int $id, DeleteCategoriaUseCase $useCase)
    {
        try{
            return $useCase->execute($id);
        }catch (\Throwable $th){
            return response(["message" =>$this->mensaje], 500);
        }
    }

    public function getById(int $id, GetCategoriaByIdUseCase $useCase)
    {
        try{
            return $useCase->execute($id);
        }catch (\Throwable $th){
            return response(["message" =>$this->mensaje], 500);
        }
    }

    public function getAll(GetAllCategoriasUseCase $useCase)
    {
        try{
            return $useCase->execute();
        }catch (\Throwable $th){
            return response(["message" =>$this->mensaje], 500);
        }
    }
}
