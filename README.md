<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Esto es lo mas Hexagonal en Arquitectura que vas a encontrar en Laravel

Prueba de Conocimientos
Parte Teórica
### Pregunta 1: Definición y Propósito
· ¿Qué es la arquitectura hexagonal y cuál es su propósito principal al ser aplicada en un proyecto de Laravel?

La arquitectura hexagonal se utiliza para desacoplar la lógica de negocio de la aplicación de Laravel y sus componentes subyacentes (la separación de preocupaciones, la modularidad y la testabilidad). Esto en teoría permite a los desarrolladores escribir código más flexible, reutilizable y fácil de probar. Se basa en la idea de encapsular la lógica de negocio central de una aplicación en un núcleo independiente.

### Pregunta 2: Componentes Claves
· Enumera y describe los componentes claves de la arquitectura hexagonal.
Núcleo: Contiene la lógica de negocio pura de la aplicación, sin dependencias de tecnologías o frameworks específicos. Se define mediante interfaces y casos de uso. 

Adaptadores de entrada: Traducen las solicitudes externas (por ejemplo, desde una API REST o una interfaz gráfica de usuario) en llamadas al núcleo. 

Adaptadores de salida: Convierten los resultados del núcleo en un formato adecuado para el consumo externo (por ejemplo, JSON, HTML, etc.). 

Puertos: Definen las interfaces de comunicación entre el núcleo y los adaptadores. Son contratos que especifican cómo interactúan los componentes sin depender de su implementación concreta. 
Frameworks y tecnologías: Se ubican en la capa exterior y proporcionan la implementación específica de los adaptadores y las tecnologías subyacentes (por ejemplo, Laravel, base de datos, etc.).

### Pregunta 3: Beneficios y Desafíos
· ¿Cuáles son los principales beneficios de implementar una arquitectura hexagonal en Laravel?

Mayor separación de preocupaciones: La lógica de negocio se separa claramente de las tecnologías y frameworks específicos, lo que mejora la modularidad y la testabilidad. 

Código más flexible y reutilizable: Los componentes de la aplicación son más independientes, lo que facilita su reutilización en diferentes proyectos. 

 Mayor facilidad de mantenimiento: La arquitectura hexagonal facilita la comprensión y el mantenimiento del código a lo largo del tiempo. 

Aplicación más escalable: La arquitectura hexagonal permite agregar nuevas funcionalidades y soportar diferentes plataformas y tecnologías sin afectar el núcleo de la aplicación. 
Pruebas unitarias más simples: Las pruebas unitarias se vuelven más simples y efectivas, ya que la lógica de negocio se puede probar de forma aislada sin necesidad de dependencias externas.
· ¿Cuáles son algunos de los desafíos que podrías enfrentar al implementarla?
Curva de aprendizaje inicial: La arquitectura hexagonal puede ser un concepto complejo para los desarrolladores que no estén familiarizados con ella. 

Mayor esfuerzo de diseño: Requiere un mayor esfuerzo de diseño inicial para identificar los componentes y las interfaces de la aplicación. 

Posible aumento de la complejidad: La arquitectura hexagonal puede aumentar la complejidad de la aplicación, especialmente en proyectos grandes. 
Necesidad de adaptadores personalizados: Puede ser necesario desarrollar adaptadores personalizados para integrar la aplicación con tecnologías o frameworks específicos.

### Pregunta 4: Diferencias con MVC
· Compara la arquitectura hexagonal con la arquitectura MVC tradicional. ¿En qué se diferencian principalmente en términos de estructura y flujo de datos?

Arquitectura Hexagonal: 

•	Las solicitudes fluyen desde las entradas (API, interfaz gráfica) hacia el núcleo a través de adaptadores de entrada.

•	El núcleo procesa las solicitudes y devuelve los resultados.

•	Los resultados se transforman en un formato adecuado para la salida a través de adaptadores de salida.

•	Las respuestas se envían al usuario a través de las salidas (API, interfaz gráfica).

Arquitectura MVC: 

•	Las solicitudes del usuario se envían al controlador.

•	El controlador interactúa con el modelo para obtener o actualizar datos.

•	El controlador selecciona la vista adecuada y le pasa los datos.

•	La vista presenta los datos al usuario y captura la interacción del usuario.

•	La interacción del usuario se envía de vuelta al controlador.


## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

You may also try the [Laravel Bootcamp](https://bootcamp.laravel.com), where you will be guided through building a modern Laravel application from scratch.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains thousands of video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the [Laravel Partners program](https://partners.laravel.com).

### Premium Partners

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[WebReinvent](https://webreinvent.com/)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Curotec](https://www.curotec.com/services/technologies/laravel/)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[DevSquad](https://devsquad.com/hire-laravel-developers)**
- **[Jump24](https://jump24.co.uk)**
- **[Redberry](https://redberry.international/laravel/)**
- **[Active Logic](https://activelogic.com)**
- **[byte5](https://byte5.de)**
- **[OP.GG](https://op.gg)**

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
